package com.itau.controllers;

import com.itau.models.Cliente;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import com.itau.services.ClienteService;

import javax.validation.Valid;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

//    {
//        "nome": "Homem Aranha"
//    }


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cliente registrarCliente(@RequestBody @Valid Cliente cliente) {
        return clienteService.salvarCliente(cliente);
    }

    @GetMapping("/{idCliente}")
    @ResponseStatus(HttpStatus.OK)
    public Cliente buscarClientePorId(@PathVariable(name = "idCliente") int id) {
        try {
            return clienteService.buscarClientePorId(id);
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }
}